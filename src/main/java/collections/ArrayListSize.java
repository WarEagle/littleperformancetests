package collections;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

@State(Scope.Thread)
public class ArrayListSize {
    @Param({"0","10","100","1000"})
//    @Param({"0","10","100","1000","10000"})
    private int iterations = 100;

    private void processing(ArrayList<String> list) {
        for (int i = 0; i < iterations; i++) {
            list.add("Entry" + new Random().nextInt(iterations));
        }
    }

    private void processingObject(ArrayList<Object> list) {
        for (int i = 0; i < iterations; i++) {
            list.add("Entry" + new Random().nextInt(iterations));
        }
    }

    private void processingPoint(ArrayList<Point> list) {
        for (int i = 0; i < iterations; i++) {
            list.add(new Point(new Random().nextInt(iterations), new Random().nextInt(iterations)));
        }
    }

    @Benchmark
    public void string() {
        ArrayList<String> list = new ArrayList<String>();
        processing(list);
    }

    @Benchmark
    public void stringPreInit() {
        ArrayList<String> list = new ArrayList<String>(iterations);
        processing(list);
    }

    @Benchmark
    public void stringPreInitOne() {
        ArrayList<String> list = new ArrayList<String>(1);
        processing(list);
    }

    @Benchmark
    public void object() {
        ArrayList list = new ArrayList();
        processingObject(list);
    }

    @Benchmark
    public void objectPreInit() {
        ArrayList list = new ArrayList(iterations);
        processingObject(list);
    }

    @Benchmark
    public void objectPreInitOne() {
        ArrayList list = new ArrayList(1);
        processingObject(list);
    }

    @Benchmark
    public void point() {
        ArrayList<Point> list = new ArrayList<>();
        processingPoint(list);
    }

    @Benchmark
    public void pointPreInit() {
        ArrayList<Point> list = new ArrayList<>(iterations);
        processingPoint(list);
    }

    @Benchmark
    public void pointPreInitOne() {
        ArrayList<Point> list = new ArrayList<>(1);
        processingPoint(list);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ArrayListSize.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(10)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
