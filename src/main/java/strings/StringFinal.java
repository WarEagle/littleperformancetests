package strings;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@State(Scope.Thread)
public class StringFinal {

    static final String FINAL_STATIC_1 = "abc1";
    static final String FINAL_STATIC_2 = "abc2";
    static final String FINAL_STATIC_3 = "abc3";
    static final String FINAL_STATIC_4 = "abc4";
    static final String FINAL_STATIC_5 = "abc5";
    static final String FINAL_STATIC_6 = "abc6";
    static final String FINAL_STATIC_7 = "abc7";
    static final String FINAL_STATIC_8 = "abc8";
    static final String FINAL_STATIC_9 = "abc9";
    static final String FINAL_STATIC_10 = "abc10";
    static final String FINAL_STATIC_11 = "abc11";
    static final String FINAL_STATIC_12 = "abc12";
    static final String FINAL_STATIC_13 = "abc13";
    static final String FINAL_STATIC_14 = "abc14";
    static final String FINAL_STATIC_15 = "abc15";

    static final String STATIC_1 = "abc1";
    static final String STATIC_2 = "abc2";
    static final String STATIC_3 = "abc3";
    static final String STATIC_4 = "abc4";
    static final String STATIC_5 = "abc5";
    static final String STATIC_6 = "abc6";
    static final String STATIC_7 = "abc7";
    static final String STATIC_8 = "abc8";
    static final String STATIC_9 = "abc9";
    static final String STATIC_10 = "abc10";
    static final String STATIC_11 = "abc11";
    static final String STATIC_12 = "abc12";
    static final String STATIC_13 = "abc13";
    static final String STATIC_14 = "abc14";
    static final String STATIC_15 = "abc15";

    String _1 = "abc1";
    String _2 = "abc2";
    String _3 = "abc3";
    String _4 = "abc4";
    String _5 = "abc5";
    String _6 = "abc6";
    String _7 = "abc7";
    String _8 = "abc8";
    String _9 = "abc9";
    String _10 = "abc10";
    String _11 = "abc11";
    String _12 = "abc12";
    String _13 = "abc13";
    String _14 = "abc14";
    String _15 = "abc15";

    final String FINAL_1 = "abc1";
    final String FINAL_2 = "abc2";
    final String FINAL_3 = "abc3";
    final String FINAL_4 = "abc4";
    final String FINAL_5 = "abc5";
    final String FINAL_6 = "abc6";
    final String FINAL_7 = "abc7";
    final String FINAL_8 = "abc8";
    final String FINAL_9 = "abc9";
    final String FINAL_10 = "abc10";
    final String FINAL_11 = "abc11";
    final String FINAL_12 = "abc12";
    final String FINAL_13 = "abc13";
    final String FINAL_14 = "abc14";
    final String FINAL_15 = "abc15";

    private String tmp = "";

    @Benchmark
    public void nonFinal() {
        tmp = "";
        tmp += _1;
        tmp += _2;
        tmp += _3;
        tmp += _4;
        tmp += _5;
        tmp += _6;
        tmp += _7;
        tmp += _8;
        tmp += _9;
        tmp += _10;
        tmp += _11;
        tmp += _12;
        tmp += _13;
        tmp += _14;
        tmp += _15;
    }

    @Benchmark
    public void finals() {
        tmp = "";
        tmp += FINAL_1;
        tmp += FINAL_2;
        tmp += FINAL_3;
        tmp += FINAL_4;
        tmp += FINAL_5;
        tmp += FINAL_6;
        tmp += FINAL_7;
        tmp += FINAL_8;
        tmp += FINAL_9;
        tmp += FINAL_10;
        tmp += FINAL_11;
        tmp += FINAL_12;
        tmp += FINAL_13;
        tmp += FINAL_14;
        tmp += FINAL_15;
    }

    @Benchmark
    public void statics() {
        tmp = "";
        tmp += STATIC_1;
        tmp += STATIC_2;
        tmp += STATIC_3;
        tmp += STATIC_4;
        tmp += STATIC_5;
        tmp += STATIC_6;
        tmp += STATIC_7;
        tmp += STATIC_8;
        tmp += STATIC_9;
        tmp += STATIC_10;
        tmp += STATIC_11;
        tmp += STATIC_12;
        tmp += STATIC_13;
        tmp += STATIC_14;
        tmp += STATIC_15;
    }

    @Benchmark
    public void staticsFinal() {
        tmp = "";
        tmp += FINAL_STATIC_1;
        tmp += FINAL_STATIC_2;
        tmp += FINAL_STATIC_3;
        tmp += FINAL_STATIC_4;
        tmp += FINAL_STATIC_5;
        tmp += FINAL_STATIC_6;
        tmp += FINAL_STATIC_9;
        tmp += FINAL_STATIC_7;
        tmp += FINAL_STATIC_8;
        tmp += FINAL_STATIC_10;
        tmp += FINAL_STATIC_11;
        tmp += FINAL_STATIC_12;
        tmp += FINAL_STATIC_13;
        tmp += FINAL_STATIC_14;
        tmp += FINAL_STATIC_15;
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringFinal.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(10)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
