package strings;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@State(Scope.Thread)
public class StringAdding {
    static final String FINAL_STATIC = "abc";
    static String STATIC = "def";
    final String FINAL = "def";
    private String tmp = "";
    private int iterations = 1000;

    @Benchmark
    public void builder() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < iterations; i++) {
            sb.append("kdf");
        }
        sb.toString();
    }

    @Benchmark
    public void buffer() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < iterations; i++) {
            sb.append("kdf");
        }
        sb.toString();
    }

    @Benchmark
    public void nonFinal() {
        tmp = "";
        for (int i = 0; i < iterations; i++) {
            tmp += "ghi";
        }
    }

    @Benchmark
    public void statics() {
        tmp = "";
        for (int i = 0; i < iterations; i++) {
            tmp += STATIC;
        }
    }

    @Benchmark
    public void finals() {
        tmp = "";
        for (int i = 0; i < iterations; i++) {
            tmp += FINAL;
        }
    }

    @Benchmark
    public void staticsFinal() {
        tmp = "";
        for (int i = 0; i < iterations; i++) {
            tmp += FINAL_STATIC;
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StringAdding.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(10)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
