package methods;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Random;

@State(Scope.Thread)
public class MethodFinal {

    private int finalTestMethod(final int parameter) {
        return parameter * new Random().nextInt(Math.abs(parameter + 1));
    }

    private int normalTestMethod(int parameter) {
        return parameter * new Random().nextInt(Math.abs(parameter + 1));
    }

    private static int staticTestMethod(int parameter) {
        return parameter * new Random().nextInt(Math.abs(parameter + 1));
    }

    @Benchmark
    public void final_() {
        int tmp = new MethodFinal().finalTestMethod(new Random().nextInt(100));
//        System.out.println(tmp);
    }

    @Benchmark
    public void statics() {
        int tmp = MethodFinal.staticTestMethod(new Random().nextInt(100));
//        System.out.println(tmp);
    }

    @Benchmark
    public void normal() {
        int tmp = new MethodFinal().normalTestMethod(new Random().nextInt(100));
//        System.out.println(tmp);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MethodFinal.class.getSimpleName())
                .warmupIterations(10)
                .measurementIterations(10)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
